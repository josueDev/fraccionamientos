//
//  ObjectComent.swift
//  TASXI
//
//  Created by Josué :D on 27/08/18.
//  Copyright © 2018 SinergiaDigital. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

private var array: NSMutableArray = NSMutableArray()

class ObjetoAlumno: NSObject {
    
    var id: String
    var student_id : String
    var nombre : String
    var asistencia : String
    var balance : String
    var rating : String
    
    init( id : String, student_id : String,nombre : String, asistencia : String, balance : String, rating : String) {
    
        self.id = id
        self.student_id = student_id
        self.nombre = nombre
        self.asistencia = asistencia
        self.balance = balance
        self.rating = rating
      
        
    }
    class func getData() -> NSMutableArray {
        return array
    }
    
    class func setData( listac : NSMutableArray ) {
        array = listac
    }
}

