import Foundation

/// Protocolo que nos identifica el tipo ViewModel
protocol ViewModelType {
    associatedtype Input
    associatedtype Output
    
    func transform(input: Input) -> Output
}
