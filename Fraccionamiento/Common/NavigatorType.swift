//
//  NavigatorType.swift
//  informacndh
//
//  Created by Lennken Group on 9/17/18.
//  Copyright © 2018 Lennken Group. All rights reserved.
//

import Foundation
import Foundation
import UIKit

/// Protocolo que nos identifica el tipo Navigator
protocol NavigatorType: AnyObject {
    var navigationController: UINavigationController? {get set}
    var storyboard: UIStoryboard! {get}
    func dismiss() -> Void
    func pop() -> Void
    func toMain(sender: Any?) -> Void
    init(_ navigationController: UINavigationController?, sender: Any?)
}
extension NavigatorType {
    var storyboard: UIStoryboard! {return nil}
    
    
    func dismiss() -> Void {
        navigationController?.dismiss(animated: true, completion: nil)
    }
    func pop() -> Void {
        navigationController?.popViewController(animated: true)
    }
    
    /// Push to Controller
    ///
    /// - Parameters:
    ///   - vc: Type of controller, The name identifier on story board, it has to be the same controller
    ///   - completion: return controller, for to do something
    func pushView<T:UIViewController>(_ vc: T.Type, completion: ((T) -> Void)?) -> Void {
        guard let storyboard = storyboard, let identifier = NSStringFromClass(vc.classForCoder()).components(separatedBy: ".").last else {
            return
        }
        let vc = storyboard.instantiateViewController(withIdentifier: identifier) as! T
        if completion != nil {
            completion!(vc)
        }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func presentView<T:UIViewController>(_ vc: T.Type, completion: ((T) -> Void)?) -> Void {
        guard let storyboard = storyboard, let identifier = NSStringFromClass(vc.classForCoder()).components(separatedBy: ".").last else {
            return
        }
        let nc =  UINavigationController()
        let vc = storyboard.instantiateViewController(withIdentifier: identifier) as! T
        if completion != nil {
            completion!(vc)
        }
        nc.modalPresentationStyle = .overFullScreen
        nc.pushViewController(vc, animated: true)
        navigationController?.present(nc, animated: true, completion: nil)
    }
    
}
