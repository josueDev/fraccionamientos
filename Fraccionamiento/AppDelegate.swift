//
//  AppDelegate.swift
//  atiende
//
//  Created by Christian Vargas Saavedra on 9/26/18.
//  Copyright © 2018 Christian Vargas Saavedra. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import MachO
//0,187,45
// (77,137,222).
var colorVerde = UIColor(displayP3Red: 0, green: 77/255, blue: 222/255, alpha: 1.0)

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var keyData = Data()
    var orientationLock = UIInterfaceOrientationMask.all

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        UIApplication.shared.statusBarStyle = .lightContent
        
        let dataPersistent = DataPersistent()
        dataPersistent.initializeDataPersistent()
        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
 
        // Override point for customization after application launch.

        
        let defaults = UserDefaults()
        if (defaults.string(forKey: Keys.KEY_64) == nil){
            var keyData = Data(count: 64)
            self.keyData = keyData
            
            _ = NSMutableData(length: 64)
            
            let result = self.keyData.withUnsafeMutableBytes {mutableBytes in
                SecRandomCopyBytes(kSecRandomDefault, keyData.count, mutableBytes)
            }
            
            if result == errSecSuccess {
                if (self.keyData.base64EncodedString().characters.count != 64 ){
                    let index = self.keyData.base64EncodedString().index(self.keyData.base64EncodedString().startIndex, offsetBy: 64)
                    defaults.set(self.keyData.base64EncodedString().substring(to: index), forKey: Keys.KEY_64)
                }else{
                    defaults.set(self.keyData.base64EncodedString(), forKey: Keys.KEY_64)
                }
                print("Key saved")
            } else {
                print("Problem generating random bytes")
            }
        }
        
     
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        if self.window!.rootViewController is SplashViewController {
            let viewController = self.window?.rootViewController as! SplashViewController
            viewController.viewDidLoad()
        }
        
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

