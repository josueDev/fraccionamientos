//
//  BtnImage.swift
//  informacndh
//
//  Created by Lennken Group on 10/1/18.
//  Copyright © 2018 Lennken Group. All rights reserved.
//

import Foundation
import UIKit
class CustomButton: UIButton {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        guard imageView != nil else {
            return
        }
        
        imageEdgeInsets = UIEdgeInsets(top: 5, left: (bounds.width - 25), bottom: 5, right: 5)
        titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: imageView!.frame.width)
        
    }
}
