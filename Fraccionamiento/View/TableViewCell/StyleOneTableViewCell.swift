//
//  StyleOneTableViewCell.swift
//  Demo
//
//  Created by Josué :D on 30/09/18.
//  Copyright © 2018 gcode. All rights reserved.
//

import UIKit

class StyleOneTableViewCell: UITableViewCell {
    
    
    @IBOutlet var labelDate: UILabel!
    
    @IBOutlet var cardView: CardView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        cardView.isUserInteractionEnabled = false
        labelDate.adjustsFontSizeToFitWidth = true
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
