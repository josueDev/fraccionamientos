//
//  IntegrationTableViewCell.swift
//  atiende
//
//  Created by Christian Vargas on 11/28/18.
//  Copyright © 2018 Christian Vargas Saavedra. All rights reserved.
//

import UIKit

class IntegrationTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var idIntegration: UILabel!
    @IBOutlet weak var descriptionIntegration: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
