//
//  MyComplaintsTableViewCell.swift
//  Demo
//
//  Created by Josué :D on 29/09/18.
//  Copyright © 2018 gcode. All rights reserved.
//

import UIKit

class MyComplaintsTableViewCell: UITableViewCell {

    @IBOutlet weak var cardView: CardView!
    @IBOutlet weak var folio: UILabel!
    @IBOutlet weak var status: CustomLabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var file: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
