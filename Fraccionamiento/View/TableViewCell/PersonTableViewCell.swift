//
//  PersonTableViewCell.swift
//  atiende
//
//  Created by Josué :D on 17/10/18.
//  Copyright © 2018 Christian Vargas Saavedra. All rights reserved.
//

import UIKit

class PersonTableViewCell: UITableViewCell {
    
    @IBOutlet var name: UILabel!
    
    
    @IBOutlet var buttonErase: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        buttonErase.imageView?.contentMode = .scaleAspectFit
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
