//
//  cellAlumnos.swift
//  atiende
//
//  Created by Josue Hernandez on 5/20/19.
//  Copyright © 2019 Christian Vargas Saavedra. All rights reserved.
//

import UIKit

class cellAlumnos: UITableViewCell {
    
    
    @IBOutlet var nombre: UILabel!
    
    @IBOutlet var status: UIImageView!
    
    @IBOutlet var button: UIButton!
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        
       

        button.imageView?.contentMode = .scaleAspectFit
        
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
