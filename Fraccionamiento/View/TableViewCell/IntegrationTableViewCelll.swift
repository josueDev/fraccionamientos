//
//  IntegrationTableViewCelll.swift
//  atiende
//
//  Created by Christian Vargas on 11/26/18.
//  Copyright © 2018 Christian Vargas Saavedra. All rights reserved.
//

import UIKit


class IntegrationTableViewCelll: UITableViewCell{
    
    
    @IBOutlet weak var idIntegration: UILabel!
    @IBOutlet weak var descriptionIntegration: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
    }
}
