//
//  DomicileOptionChildController.swift
//  atiende
//
//  Created by Josué Gustavo Hernández Villa on 07/12/18.
//  Copyright © 2018 Christian Vargas Saavedra. All rights reserved.
//

import UIKit

var optionAdditionalDataDomicileMain = UISwitch()

class DomicileOptionChildController: UIViewController {
    
    
    @IBOutlet var optionAdditionalDataDomicile: UISwitch!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        optionAdditionalDataDomicileMain = optionAdditionalDataDomicile

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
