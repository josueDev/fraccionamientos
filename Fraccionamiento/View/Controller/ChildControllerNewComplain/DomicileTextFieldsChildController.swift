//
//  DomicileTextFieldsChildController.swift
//  atiende
//
//  Created by Josué Gustavo Hernández Villa on 07/12/18.
//  Copyright © 2018 Christian Vargas Saavedra. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField


var textFieldColonyMain: SkyFloatingLabelTextField!
var textFieldInteriorNumberMain : SkyFloatingLabelTextField!
var textFieldExteriorNumberMain : SkyFloatingLabelTextField!
var textFieldStreetAddreesMain : SkyFloatingLabelTextField!

class DomicileTextFieldsChildController: UIViewController {
    
    
    @IBOutlet var textFieldStreetAddrees: SkyFloatingLabelTextField!
    @IBOutlet var textFieldColony: SkyFloatingLabelTextField!
    @IBOutlet var textFieldExteriorNumber: SkyFloatingLabelTextField!
    @IBOutlet var textFieldInteriorNumber: SkyFloatingLabelTextField!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        textFieldColonyMain = textFieldColony
        textFieldInteriorNumberMain = textFieldInteriorNumber
        textFieldExteriorNumberMain = textFieldExteriorNumber
        textFieldStreetAddreesMain = textFieldStreetAddrees
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
