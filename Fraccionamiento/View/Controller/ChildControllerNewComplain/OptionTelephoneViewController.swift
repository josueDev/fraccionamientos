//
//  OptionTelephoneViewController.swift
//  atiende
//
//  Created by Josue Hernandez on 11/26/18.
//  Copyright © 2018 Christian Vargas Saavedra. All rights reserved.
//

import UIKit

var buttonLandlineMain = UIButton()
var buttonCellPhoneMain = UIButton()
var labelLandLineMain = UILabel()
var labelCellPhoneMain =  UILabel()

class OptionTelephoneViewController: UIViewController {
    
    
    @IBOutlet var buttonLandline: UIButton!
    @IBOutlet var buttonCellPhone: UIButton!
    
    @IBOutlet var labelLandLine: UILabel!
    @IBOutlet var labelCellPhone: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        buttonLandlineMain = buttonLandline
        buttonCellPhoneMain = buttonCellPhone
        
        labelLandLineMain = labelLandLine
        labelCellPhoneMain = labelCellPhone
        
        buttonLandlineMain.setImage(UIImage(named: "option"), for:.normal)
        buttonCellPhoneMain.setImage(UIImage(named: "option"), for:.normal)
        

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
