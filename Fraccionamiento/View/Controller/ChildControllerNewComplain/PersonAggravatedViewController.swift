//
//  PersonAggravatedViewController.swift
//  atiende
//
//  Created by Josué Gustavo Hernández Villa on 19/11/18.
//  Copyright © 2018 Christian Vargas Saavedra. All rights reserved.
//

import UIKit

var switchPersonAggravatedMain = UISwitch()

class PersonAggravatedViewController: UIViewController {
    
    @IBOutlet var switchPersonAggravated: UISwitch!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        switchPersonAggravatedMain = self.switchPersonAggravated

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
