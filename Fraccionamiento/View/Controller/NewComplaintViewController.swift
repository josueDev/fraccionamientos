//
//  NewComplaintViewController.swift
//  Demo
//
//  Created by Josué :D on 27/09/18.
//  Copyright © 2018 gcode. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Gallery
import Lightbox
import AVFoundation
import AVKit
import SVProgressHUD

import Alamofire

import SkyFloatingLabelTextField
import UIFloatLabelTextView
import MobileCoreServices
import SwiftSpinner

var uploadComplaintPending = false
var parentLoginViewController = UIViewController()

class NewComplaintViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    
    @IBOutlet var tableView: UITableView!
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "Mis recibos"
        
        self.navigationController?.navigationBar.tintColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1) // title and icon back
        self.navigationController!.navigationBar.titleTextAttributes = [ NSAttributedString.Key.foregroundColor: UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)] // titleBar
        self.navigationController?.navigationBar.barTintColor = colorVerde
        
        UIApplication.shared.statusBarStyle = .lightContent
        
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        
    }
    
    @IBAction func openMenu(_ sender: Any) {
        sideMenuController?.showLeftView(animated: true, completionHandler: nil)
    }
    
    
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews();
        print("viewWillLayoutSubviews")
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: "CeldaRecibo", bundle: nil), forCellReuseIdentifier: "CeldaRecibo")
        tableView.delegate = self
        tableView.dataSource = self
//        let ppl_code_id = String(describing: UserDefaults.standard.value(forKey: DataPersistent.ppl_code_id)!)
//        let name = String(describing: UserDefaults.standard.value(forKey: DataPersistent.name)!)
    
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView,heightForRowAt indexPath: IndexPath) -> CGFloat
    {
       return 121
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let CeldaRecibo = tableView.dequeueReusableCell(withIdentifier: "CeldaRecibo", for: indexPath) as! CeldaRecibo
        return  CeldaRecibo
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true);
//        let producto : MapaDetalleViewModel  = MapaDetalleViewModel.getProductos().object(at: indexPath.row) as! MapaDetalleViewModel
//
//        if (producto.seleccionado)
//        {
//            producto.seleccionado = false
//        }
//        else
//        {
//            producto.seleccionado = true
//        }
//
//        UIView.setAnimationsEnabled(false)
//        self.tableView.beginUpdates()
//
//        let indexPosition = IndexPath(row: indexPath.row, section: 0)
//        tableView.reloadRows(at: [indexPosition], with: .automatic)
//        self.tableView.endUpdates()
        
    }
    
    
    
    
}





