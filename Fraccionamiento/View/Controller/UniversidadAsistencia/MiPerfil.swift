//
//  MiPerfil.swift
//  atiende
//
//  Created by Josue Hernandez on 5/20/19.
//  Copyright © 2019 Christian Vargas Saavedra. All rights reserved.
//

import UIKit
import LGSideMenuController

class MiPerfil: UIViewController {
    
    @IBOutlet var id: UILabel!
    
    @IBOutlet var nombre: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //sideMenuController?.leftViewBackgroundColor = UIColor(red: 0.0, green: 0.5, blue: 1.0, alpha: 0.1)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil;
    }
    
    @IBAction func onOpenMenuClick(_ sender: UIBarButtonItem) {
        sideMenuController?.showLeftView(animated: true, completionHandler: nil)
    }
    
    public func onItemMenuClick(itemMenu: Int){
        sideMenuController?.hideLeftView()
        switch itemMenu {
        case 0:
            let vc = UIStoryboard.init(name: "NewComplaintViewController", bundle: Bundle.main).instantiateViewController(withIdentifier: "NewComplaintViewController") as? NewComplaintViewController
            navigationController?.present(vc!, animated: true, completion: nil)
        default:
            break
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
