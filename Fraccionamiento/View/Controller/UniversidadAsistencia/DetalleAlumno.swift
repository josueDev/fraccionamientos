//
//  DetalleAlumno.swift
//  atiende
//
//  Created by Josue Hernandez on 6/7/19.
//  Copyright © 2019 Christian Vargas Saavedra. All rights reserved.
//

import UIKit

class DetalleAlumno: UIViewController {

    
    @IBOutlet var nombre: UILabel!
    
    @IBOutlet var monto: UILabel!
    
    @IBOutlet var documento: UILabel!
    
    @IBOutlet var calificaciones: UILabel!
    
    @IBOutlet var porcentaje: UILabel!
    
    var positionAlumno: Int!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.setNavigationBarItem()
        
        //leftPresenter = LeftPresenter(delegate: self)
        
        UIView.animate(withDuration: 0.2, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
            
        
            self.navigationItem.title = "Perfil de Alumno"
        
            
            UIApplication.shared.statusBarStyle = .lightContent
            
        }, completion: nil)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let alumno : ObjetoAlumno = ObjetoAlumno.getData().object(at: positionAlumno) as! ObjetoAlumno
        nombre.text = alumno.nombre
        nombre.adjustsFontSizeToFitWidth = true
        
        
        monto.text = "Deuda $: \(alumno.balance)"
        monto.adjustsFontSizeToFitWidth = true
        
        documento.text = "Sin bloqueo"
        monto.adjustsFontSizeToFitWidth = true
        
        calificaciones.text = "Parcial 1: \(alumno.rating)"
        monto.adjustsFontSizeToFitWidth = true
        
        porcentaje.text = "98% de Asistencia"
        monto.adjustsFontSizeToFitWidth = true
        
        
        

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
