//
//  MisMensajes.swift
//  Fraccionamiento
//
//  Created by Josue Hernandez on 6/14/19.
//  Copyright © 2019 Christian Vargas Saavedra. All rights reserved.
//

import UIKit

class MisMensajes: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet var tableView: UITableView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "Mis mensajes"
        
        self.navigationController?.navigationBar.tintColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1) // title and icon back
        self.navigationController!.navigationBar.titleTextAttributes = [ NSAttributedString.Key.foregroundColor: UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)] // titleBar
        self.navigationController?.navigationBar.barTintColor = colorVerde
        
        UIApplication.shared.statusBarStyle = .lightContent
        
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "CellMensajes", bundle: nil), forCellReuseIdentifier: "CellMensajes")

        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView,heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let CellMensajes = tableView.dequeueReusableCell(withIdentifier: "CellMensajes", for: indexPath) as! CellMensajes
        return  CellMensajes
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true);
        
      
        
        //        let producto : MapaDetalleViewModel  = MapaDetalleViewModel.getProductos().object(at: indexPath.row) as! MapaDetalleViewModel
        //
        //        if (producto.seleccionado)
        //        {
        //            producto.seleccionado = false
        //        }
        //        else
        //        {
        //            producto.seleccionado = true
        //        }
        //
        //        UIView.setAnimationsEnabled(false)
        //        self.tableView.beginUpdates()
        //
        //        let indexPosition = IndexPath(row: indexPath.row, section: 0)
        //        tableView.reloadRows(at: [indexPosition], with: .automatic)
        //        self.tableView.endUpdates()
        
    }
    
    
    @IBAction func openMenu(_ sender: Any) {
        sideMenuController?.showLeftView(animated: true, completionHandler: nil)
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
