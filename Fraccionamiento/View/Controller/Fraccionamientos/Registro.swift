//
//  Registro.swift
//  Fraccionamiento
//
//  Created by Josue Hernandez on 6/12/19.
//  Copyright © 2019 Christian Vargas Saavedra. All rights reserved.
//

import UIKit
import SwiftSpinner
import Alamofire
import SwiftyJSON
import PKHUD

class Registro: UIViewController {
    
    
    @IBOutlet var campoNombre: DesignableUITextField!
    
    @IBOutlet var campoNumeroCasa: DesignableUITextField!
    
    @IBOutlet var campoCalle: DesignableUITextField!
    
    @IBOutlet var campoColonia: DesignableUITextField!
    
    @IBOutlet var campoCorreo: DesignableUITextField!
    
    @IBOutlet var campoCelular: DesignableUITextField!
    
    @IBOutlet var campoContraseña: DesignableUITextField!
    
    @IBOutlet var campoContraseña2: DesignableUITextField!
    

    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "Registro"
        
        self.navigationController?.navigationBar.tintColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1) // title and icon back
        self.navigationController!.navigationBar.titleTextAttributes = [ NSAttributedString.Key.foregroundColor: UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)] // titleBar
        self.navigationController?.navigationBar.barTintColor = colorVerde
        
        UIApplication.shared.statusBarStyle = .lightContent
        
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        
    }
    
    lazy var loginViewControllerLoaded: LoginViewController = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        var view = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        
        return view
    }()

    lazy var menuViewControllerLoaded: MenuViewController = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        var view = storyboard.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        
        return view
    }()
    
    lazy var mainViewControllerLoaded: MainViewController = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        var view = storyboard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
        
        return view
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        campoNombre.text = "Josue G"
        campoNumeroCasa.text = "36"
        campoCalle.text = "Avila camacho"
        campoColonia.text = "Tamborell"
        campoCorreo.text = "josue@gmail.com"
        campoCelular.text = "2288582567"
        campoContraseña.text = "123"
        campoContraseña2.text = "123"
        
        // Do any additional setup after loading the view.
    }

    
    
    @IBAction func actionRegistrar(_ sender: Any) {
        loadHome()
    }
    
    func loadHome()
    {
//        let viewController = mainViewControllerLoaded
//
//        let navigationController = NavigationViewController(rootViewController: viewController)
//        navigationController.navigationBar.tintColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1) // title and icon back
//        sideMenuController?.leftViewBackgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1) // title and icon back
//
//        let mainViewController = menuViewControllerLoaded
//        mainViewController.rootViewController = navigationController
//        mainViewController.setup(type: 0)
//
//        let window = UIApplication.shared.delegate!.window!!
//        window.rootViewController = mainViewController
//
//        UIView.transition(with: window, duration: 0.3, options: [.transitionCrossDissolve], animations: nil, completion: nil)
    }
    
    
    @IBAction func close(_ sender: Any) {
        let viewController = loginViewControllerLoaded
        
        let navigationController = NavigationViewController(rootViewController: viewController)
        
        let mainViewController = loginViewControllerLoaded
        //mainViewController.rootViewController = navigationController
        //mainViewController.setup(type: 0)
        
        let window = UIApplication.shared.delegate!.window!!
        window.rootViewController = mainViewController
        
        UIView.transition(with: window, duration: 0.3, options: [.transitionCrossDissolve], animations: nil, completion: nil)
    }
    
    @objc func getRegistro()
    {
        //HUD.show(.progress)
        
        
        //let id = String(describing: UserDefaults.standard.value(forKey: DataPersistent.id)!)
        //let idViajePorNotificacion = String(describing: UserDefaults.standard.value(forKey: DataPersistent.id_viaje)!)
        

        
        let parameters: Parameters = [
            "kr3ator_err": "1",
            "ac": "1",
            "rol" : "1",
            "name" : campoNombre.text!,
            "last_name" : campoNombre.text!,
            "usr_pass" :campoContraseña.text!,
            "phone" : campoCelular.text!,
            "cel_phone" : campoCelular.text!,
            "home_location" : campoCalle.text!,
            "number" : campoNumeroCasa.text!,
            "custom_payment" : "500",
            
        ]
        

        
        //print("\(ApiDefinition.WS_GENERAL)?ac=1&UsrName=\(userTextField.text!)&UsrPass=\(passTextField.text!)")
        Alamofire.request(ApiDefinition.WS_GENERAL, method: .post, parameters : parameters, encoding: URLEncoding.default).validate().responseJSON { response in
            
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                
                
                SwiftSpinner.hide()
                print("WS_GENERAL: \(json)")
                //let resultado = json["estado"]
                let res = String(describing: json["estado"])
                print("RES \(res)")
                if ((res) == "1")
                {
                    
                    let usuario = json["Usuario"]
                    
                    let id = String(describing: usuario["id"])
                    let token = String(describing:usuario["token"])
                    let rol = String(describing:usuario["rol"])
                    let name = String(describing: usuario["name"])
                    let last_name = String(describing:usuario["last_name"])
                    let email = String(describing:usuario["email"])
                    let phone = String(describing: usuario["phone"])
                    let cel_phone = String(describing:usuario["cel_phone"])
                    let home_location = String(describing:usuario["home_location"])
                    let number = String(describing: usuario["number"])
                    let custom_payment = String(describing:usuario["custom_payment"])
                    let status = String(describing:usuario["status"])
                    
                    
                    UserDefaults.standard.setValue("true", forKey: DataPersistent.TERMINAL_LOGIN)
                    
                    UserDefaults.standard.setValue(id, forKey: DataPersistent.id)
                    UserDefaults.standard.setValue(token, forKey: DataPersistent.token)
                    UserDefaults.standard.setValue(rol, forKey: DataPersistent.rol)
                    UserDefaults.standard.setValue(last_name, forKey: DataPersistent.last_name)
                    UserDefaults.standard.setValue(email, forKey: DataPersistent.email)
                    UserDefaults.standard.setValue(phone, forKey: DataPersistent.phone)
                    UserDefaults.standard.setValue(cel_phone, forKey: DataPersistent.cel_phone)
                    UserDefaults.standard.setValue(home_location, forKey: DataPersistent.home_location)
                    UserDefaults.standard.setValue(number, forKey: DataPersistent.number)
                    UserDefaults.standard.setValue(custom_payment, forKey: DataPersistent.custom_payment)
                    UserDefaults.standard.setValue(status, forKey: DataPersistent.status)
                    UserDefaults.standard.synchronize()
                    
                    self.loadHome()
                    //PKHUD.sharedHUD.hide(afterDelay: 0.5) { success in
                    
                    
                    //}
                }
                else
                {
                    
                    //PKHUD.sharedHUD.hide(afterDelay: 0.5) { success in
                    let alert = UIAlertController(title: "U. Insurgentes", message: "No se ha podido registrar el usuario", preferredStyle: .alert)
                    let actionOK : UIAlertAction = UIAlertAction(title: "Deacuerdo", style: .default, handler: nil)
                    alert.addAction(actionOK)
                    self.present(alert, animated: true, completion: nil)
                    //}
                }
            case .failure(let error):
                print(error)
                
                SwiftSpinner.hide()
                //PKHUD.sharedHUD.hide(afterDelay: 0.5) { success in
                let alert = UIAlertController(title: "U. Insurgentes", message: "No se ha podido registrar el usuario", preferredStyle: .alert)
                let actionOK : UIAlertAction = UIAlertAction(title: "Deacuerdo", style: .default, handler: nil)
                alert.addAction(actionOK)
                self.present(alert, animated: true, completion: nil)
                //}
            }
        }
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
