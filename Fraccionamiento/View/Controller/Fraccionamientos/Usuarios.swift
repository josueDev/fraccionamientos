//
//  Usuarios.swift
//  Fraccionamiento
//
//  Created by Josue Hernandez on 6/14/19.
//  Copyright © 2019 Christian Vargas Saavedra. All rights reserved.
//

import UIKit

class Usuarios: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet var tableView: UITableView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "Usuarios"
        
        self.navigationController?.navigationBar.tintColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1) // title and icon back
        self.navigationController!.navigationBar.titleTextAttributes = [ NSAttributedString.Key.foregroundColor: UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)] // titleBar
        self.navigationController?.navigationBar.barTintColor = colorVerde
        
        UIApplication.shared.statusBarStyle = .lightContent
        
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        
    }
    
//    @IBAction func openMenu(_ sender: Any) {
//        sideMenuController?.showLeftView(animated: true, completionHandler: nil)
//    }
    
    
    @IBAction func openMenu(_ sender: Any) {
        sideMenuController?.showLeftView(animated: true, completionHandler: nil)
    }
    
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews();
        print("viewWillLayoutSubviews")
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
         tableView.register(UINib(nibName: "CeldaUsuarioDetalle", bundle: nil), forCellReuseIdentifier: "CeldaUsuarioDetalle")

        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView,heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 121
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let CeldaRecibo = tableView.dequeueReusableCell(withIdentifier: "CeldaUsuarioDetalle", for: indexPath) as! CeldaUsuarioDetalle
        return  CeldaRecibo
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true);
        
        let alert = UIAlertController(title: "Fraccionamientos", message: "Por favor, eliga la acción que desea realizar", preferredStyle: .actionSheet)
        
        
        
        
        alert.addAction(UIAlertAction(title: "Pagar", style: .default, handler: { action in
            
            let alert = UIAlertController(title: "Fraccionamientos", message: "Usted recibirà un monto de $500 por parte de Josué Hernandez, domicilio Canal de Miramontes 2769, Colonia Coyoacan, ¿Desea confirmar el pago?", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Confirmar Pago", style: UIAlertAction.Style.default, handler: { action in
                
                //self.alertPleaseWait()
            }))
            
            alert.addAction(UIAlertAction(title: "Cancelar", style: UIAlertAction.Style.destructive, handler: { action in
                
                //self.requestCancelTravel();
                
            }))
            
            self.present(alert, animated: true, completion: nil)
            
            
        }))
        
        alert.addAction(UIAlertAction(title: "Editar Usuario", style: .default, handler: { action in
            
          
            
        }))
        
        alert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: { action in
            
            
        }))
        
        self.present(alert, animated: true)
    
        //        let producto : MapaDetalleViewModel  = MapaDetalleViewModel.getProductos().object(at: indexPath.row) as! MapaDetalleViewModel
        //
        //        if (producto.seleccionado)
        //        {
        //            producto.seleccionado = false
        //        }
        //        else
        //        {
        //            producto.seleccionado = true
        //        }
        //
        //        UIView.setAnimationsEnabled(false)
        //        self.tableView.beginUpdates()
        //
        //        let indexPosition = IndexPath(row: indexPath.row, section: 0)
        //        tableView.reloadRows(at: [indexPosition], with: .automatic)
        //        self.tableView.endUpdates()
        
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
