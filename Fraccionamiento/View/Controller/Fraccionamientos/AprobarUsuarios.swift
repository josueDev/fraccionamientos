

import UIKit

class AprobarUsuarios: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet var tableView: UITableView!
    
    
    @IBAction func mactionmenu(_ sender: Any) {
         sideMenuController?.showLeftView(animated: true, completionHandler: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.title = "Aprobar usuarios"
        
        self.navigationController?.navigationBar.tintColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1) // title and icon back
        self.navigationController!.navigationBar.titleTextAttributes = [ NSAttributedString.Key.foregroundColor: UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)] // titleBar
        self.navigationController?.navigationBar.barTintColor = colorVerde
        
        UIApplication.shared.statusBarStyle = .lightContent
        
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "CeldaUsuarioDetalle", bundle: nil), forCellReuseIdentifier: "CeldaUsuarioDetalle")
        
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView,heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let CeldaUsuarioDetalle = tableView.dequeueReusableCell(withIdentifier: "CeldaUsuarioDetalle", for: indexPath) as! CeldaUsuarioDetalle
        return  CeldaUsuarioDetalle
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true);
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "AprobarUsuarioDetalle") as? AprobarUsuarioDetalle
        self.navigationController?.pushViewController(vc!, animated: true)
        
        
        
        //        let producto : MapaDetalleViewModel  = MapaDetalleViewModel.getProductos().object(at: indexPath.row) as! MapaDetalleViewModel
        //
        //        if (producto.seleccionado)
        //        {
        //            producto.seleccionado = false
        //        }
        //        else
        //        {
        //            producto.seleccionado = true
        //        }
        //
        //        UIView.setAnimationsEnabled(false)
        //        self.tableView.beginUpdates()
        //
        //        let indexPosition = IndexPath(row: indexPath.row, section: 0)
        //        tableView.reloadRows(at: [indexPosition], with: .automatic)
        //        self.tableView.endUpdates()
        
    }
    
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

