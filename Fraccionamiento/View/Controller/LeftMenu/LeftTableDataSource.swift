//
//  LeftTableDataSource.swift
//  atiende
//
//  Created by Christian Vargas on 11/6/18.
//  Copyright © 2018 Christian Vargas Saavedra. All rights reserved.
//

import UIKit
import ViewAnimator

class LeftTableDataSource: NSObject, UITableViewDataSource, UITableViewDelegate {
    
    let fromAnimation = AnimationType.from(direction: .right, offset: 30.0)
    let zoomAnimation = AnimationType.zoom(scale: 0.2)
    
    var titlesArray: [String] = []
    var imagesArray: [String] = []

    var leftTableDataSourceDelegate: LeftTableDataSourceDelegate?
    var leftTableView: UITableView?

    private let animations = [AnimationType.from(direction: .bottom, offset: 30.0)]
    private let activityIndicator = UIActivityIndicatorView(style: .gray)

    init (withTableView leftTableView: UITableView?, leftTableDataSourceDelegate: LeftTableDataSourceDelegate){
        self.leftTableDataSourceDelegate = leftTableDataSourceDelegate
        self.leftTableView = leftTableView

    }

    func update(_ titlesArray: [String], _ imagesArray: [String]){
        self.titlesArray = titlesArray
        self.imagesArray = imagesArray
        leftTableView?.reloadData()
        UIView.animate(views: (leftTableView?.visibleCells)!,
                       animations: [fromAnimation, zoomAnimation], delay: 0.3)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titlesArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "LeftViewCell", for: indexPath) as! LeftViewCell
        
        cell.titleMenu.setTitle(titlesArray[indexPath.row], for: .normal)
        cell.imageView!.image = UIImage(named: imagesArray[indexPath.row])!
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        leftTableDataSourceDelegate?.onItemMenuClick(itemMenu: indexPath.row)
    }   
}
