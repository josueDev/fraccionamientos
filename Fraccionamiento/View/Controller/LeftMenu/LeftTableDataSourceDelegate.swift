//
//  LeftTableDataSourceDelegate.swift
//  atiende
//
//  Created by Christian Vargas on 11/6/18.
//  Copyright © 2018 Christian Vargas Saavedra. All rights reserved.
//

import UIKit

public protocol LeftTableDataSourceDelegate: NSObjectProtocol{
    
    func onItemMenuClick(itemMenu: Int)
}
