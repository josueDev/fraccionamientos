//
//  LeftViewController.swift
//  atiende
//
//  Created by Christian Vargas on 11/4/18.
//  Copyright © 2018 Christian Vargas Saavedra. All rights reserved.
//

import UIKit
import SwiftSpinner

class LeftViewController: UIViewController, LeftTableDataSourceDelegate {
    //let service: NetUseCaseProvider! = NetUseCaseProvider()
    //let rmservice: RMUseCaseProvider! = RMUseCaseProvider()
    var nav: NavigationViewController!
    private let titlesArray = ["Mi perfil",
                               "Mis recibos",
                               "Mis mensajes",
                               "Lista de Usuarios",
                               "Nuevo Usuario",
                               "Aprobar Usuarios",
                               "Cerrar Sesión",
                                ]
    
    private let imagesArray = ["ImageMenuInicio",
                               "ImageMenuInicio",
                               "ImageMenuInicio",
                               "ImageMenuAcercaDe",
                               "ImageMenuAcercaDe",
                               "ImageMenuAcercaDe",
                               "ImageMenuAcercaDe"
                               ]
    
    lazy var newComplaintViewController: NewComplaintViewController = {
        let storyboard = UIStoryboard(name: "NewComplaintViewController", bundle: Bundle.main)
        
        var view = storyboard.instantiateViewController(withIdentifier: "NewComplaintViewController") as! NewComplaintViewController
        
        return view
    }()
    
    lazy var loginViewController: LoginViewController = {
        let storyboard = UIStoryboard(name: "LoginViewController", bundle: Bundle.main)
        
        var view = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        
        return view
    }()
    
    lazy var mainViewControllerLoaded: MainViewController = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        var view = storyboard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
        
        return view
    }()
    
    lazy var menuViewControllerLoaded: MenuViewController = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        var view = storyboard.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        
        return view
    }()
    
    lazy var usuariosViewController: Usuarios = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        var view = storyboard.instantiateViewController(withIdentifier: "Usuarios") as! Usuarios
        return view
    }()
    
    lazy var mensajesViewController: MisMensajes = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        var view = storyboard.instantiateViewController(withIdentifier: "MisMensajes") as! MisMensajes
        return view
    }()
    

    lazy var MiPerfilViewController: MiPerfil = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        var view = storyboard.instantiateViewController(withIdentifier: "MiPerfil") as! MiPerfil
        return view
    }()
    
    
    lazy var nuevoUsuarioViewController: NuevoUsuario = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        var view = storyboard.instantiateViewController(withIdentifier: "NuevoUsuario") as! NuevoUsuario
        return view
    }()
    
    lazy var aprobarUsuariosViewController: AprobarUsuarios = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        var view = storyboard.instantiateViewController(withIdentifier: "AprobarUsuarios") as! AprobarUsuarios
        return view
    }()
    
   
    
    
    
    var leftTableDataSource: LeftTableDataSource?
    
    @IBOutlet weak var sessionButton: UIButton!
    @IBOutlet weak var leftTableView: UITableView!
    
    //var leftPresenter: LeftPresenter?
    var sessionExist: Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //leftPresenter = LeftPresenter(delegate: self)
        createTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("ok")
    }
    
    func createTableView(){
        leftTableDataSource = LeftTableDataSource(withTableView: self.leftTableView, leftTableDataSourceDelegate: self)
        leftTableView.dataSource = leftTableDataSource!
        leftTableView.delegate = leftTableDataSource!
        leftTableDataSource?.update(titlesArray, imagesArray)
        //sessionExist = leftPresenter?.sessionExist()
        //sessionButton.setTitle(sessionExist! ? "Cerrar sesión" :  "Iniciar sesión", for: .normal)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return .fade
    }
    @IBAction func onLoginActionClick(_ sender: Any) {
        
        let mainViewController = sideMenuController!

        mainViewController.hideLeftView(animated: true, completionHandler: nil)

        if !sessionExist! {
            openLogin()
        }else{
            closeSession()
        }
        
         mainViewController.hideLeftView(animated: true, delay: 0.0, completionHandler: nil)
        
    }
    
    func openLogin(){
        let window = UIApplication.shared.delegate!.window!!
        window.rootViewController = loginViewController
        
        UIView.transition(with: window, duration: 0.3, options: [.transitionCrossDissolve], animations: nil, completion: nil)
    }

    func closeSession(){
        let alertController = UIAlertController(title: "Cerrar sesión", message: "¿Quiere cerrar sesión en CNDH Atiende?", preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title:  "Aceptar", style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            SwiftSpinner.show("Cerrando sesión")
            //self.leftPresenter!.logout()
        }
        
        let DestructiveAction = UIAlertAction(title: "Cancelar", style: UIAlertAction.Style.destructive) { (result : UIAlertAction) -> Void in
           
        }
        
        alertController.addAction(okAction)
        alertController.addAction(DestructiveAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func onSuccessLogout() {
        let viewController = mainViewControllerLoaded
        
        
        nav = NavigationViewController(rootViewController: viewController)
        
        let mainViewController = menuViewControllerLoaded
        mainViewController.rootViewController = nav
        mainViewController.setup(type: 0)
        
        let window = UIApplication.shared.delegate!.window!!
        window.rootViewController = mainViewController
        
        UIView.transition(with: window, duration: 0.3, options: [.transitionCrossDissolve], animations: nil, completion: nil)
    }
    
    
    func onItemMenuClick(itemMenu: Int) {
        let mainViewController = sideMenuController!
        
        switch itemMenu {
        case 0:
            let viewController = mainViewControllerLoaded
            openController(controller: viewController)
        case 1:
            let viewController = newComplaintViewController
            openController(controller: viewController)
        case 2:
            print("2")
            //UserDefaults.standard.setValue("false", forKey: DataPersistent.TERMINAL_LOGIN)

            //loadLogin()
            
            let viewController = mensajesViewController
            openController(controller: viewController)
            
        case 3:
           print("e")
           let viewController = usuariosViewController
           openController(controller: viewController)
            break
        case 4:
            print("4")
            let viewController = nuevoUsuarioViewController
            openController(controller: viewController)
        case 5:
            print("5")
            let viewController = aprobarUsuariosViewController
            openController(controller: viewController)
        case 6:
             print("6")
      
            let viewController = loginViewController
            
            let navigationController = NavigationViewController(rootViewController: viewController)
            
            let mainViewController = loginViewController
            //mainViewController.rootViewController = navigationController
            //mainViewController.setup(type: 0)
            
            let window = UIApplication.shared.delegate!.window!!
            window.rootViewController = mainViewController
            
            UIView.transition(with: window, duration: 0.3, options: [.transitionCrossDissolve], animations: nil, completion: nil)
            
            break
        default:
            print("default")
        }
     
    }
    
    func openController(controller: UIViewController){

        let viewController = controller
        
        let navigationController = NavigationViewController(rootViewController: viewController)
        
        let mainViewController = menuViewControllerLoaded
        mainViewController.rootViewController = navigationController
        mainViewController.setup(type: 0)
        
        let window = UIApplication.shared.delegate!.window!!
        window.rootViewController = mainViewController
        
        UIView.transition(with: window, duration: 0.3, options: [.transitionCrossDissolve], animations: nil, completion: nil)
    }
    
    func loadLogin()
    {
        let viewController = loginViewController
        
        let navigationController = NavigationViewController(rootViewController: viewController)
        
        
        let mainViewController = loginViewController
        //mainViewController.rootViewController = navigationController
        //mainViewController.setup(type: 0)
        
        let window = UIApplication.shared.delegate!.window!!
        window.rootViewController = mainViewController
        
        UIView.transition(with: window, duration: 0.3, options: [.transitionCrossDissolve], animations: nil, completion: nil)
    }
    
    
    
}
