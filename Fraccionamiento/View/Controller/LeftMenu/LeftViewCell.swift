//
//  LeftViewCell.swift
//  atiende
//
//  Created by Christian Vargas on 11/6/18.
//  Copyright © 2018 Christian Vargas Saavedra. All rights reserved.
//

import UIKit


class LeftViewCell: UITableViewCell {
    
    
    @IBOutlet weak var titleMenu: UIButton!
    @IBOutlet weak var imageMenu: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        backgroundColor = .clear
    }
    
    
}
