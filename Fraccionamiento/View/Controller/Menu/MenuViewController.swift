//
//  MenuTabViewController.swift
//  atiende
//
//  Created by Christian Vargas on 9/29/18.
//  Copyright © 2018 Christian Vargas Saavedra. All rights reserved.
//

import UIKit
import LGSideMenuController.LGSideMenuController
import LGSideMenuController

class MenuViewController: LGSideMenuController{

    private var type: UInt?
    
    lazy var leftViewControllerLoaded: LeftViewController = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        var view = storyboard.instantiateViewController(withIdentifier: "LeftViewController") as! LeftViewController
        return view
    }()
    
    func setup(type: UInt) {
        self.type = type
        //leftViewWidth = 300.0;
        leftViewPresentationStyle = .slideBelow
        //sideMenuController?.leftViewBackgroundColor =  UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.leftViewBackgroundColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
    }
    
    override func leftViewWillLayoutSubviews(with size: CGSize) {
        super.leftViewWillLayoutSubviews(with: size)
        
        if !isLeftViewStatusBarHidden {
            leftView?.frame = CGRect(x: 0.0, y: 20.0, width: size.width, height: size.height - 20.0)
        }
    }
    
    override var isLeftViewStatusBarHidden: Bool {
        get {
            return super.isLeftViewStatusBarHidden
        }
        
        set {
            super.isLeftViewStatusBarHidden = newValue
        }
    }
    
    
    deinit {
        print("MainViewController deinitialized")
    }

}
