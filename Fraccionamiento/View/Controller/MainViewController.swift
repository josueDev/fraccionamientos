//
//  ViewController.swift
//  atiende
//
//  Created by Christian Vargas Saavedra on 9/26/18.
//  Copyright © 2018 Christian Vargas Saavedra. All rights reserved.


import UIKit
import ViewAnimator
import LGSideMenuController
import Alamofire
import SwiftyJSON
import PKHUD

class MainViewController: UIViewController, UITableViewDataSource, UITableViewDelegate{

    
    @IBOutlet var buttonAceptarLista: UIButton!
    
    @IBOutlet var headerTitulo: UILabel!
    
    
   
    func onSuccessLogout() {
        
    }
    
    //let service: NetUseCaseProvider! = NetUseCaseProvider()
    //let rmservice: RMUseCaseProvider! = RMUseCaseProvider()
    
    @IBOutlet var viewsCollection: [UIView]!
    @IBOutlet var viewsCollectionMenu: [UIView]!

    var status_clase = ""
    var token_clase = ""
    var class_name_clase = ""
    var teacher_id_clase = ""
    var teacher_name_clase = ""
    
    //var leftPresenter: LeftPresenter?
    
    lazy var newComplaintViewController: NewComplaintViewController = {
        let storyboard = UIStoryboard(name: "NewComplaintViewController", bundle: Bundle.main)
        
        var view = storyboard.instantiateViewController(withIdentifier: "NewComplaintViewController") as! NewComplaintViewController
        
        return view
    }()
    
    lazy var menuViewControllerLoaded: MenuViewController = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        var view = storyboard.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        
        return view
    }()
    
    
    lazy var loginViewController: LoginViewController = {
        let storyboard = UIStoryboard(name: "LoginViewController", bundle: Bundle.main)
        
        var view = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        
        return view
    }()
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.setNavigationBarItem()
        
        //leftPresenter = LeftPresenter(delegate: self)
        
        UIView.animate(withDuration: 0.2, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
            
            self.navigationController?.navigationBar.tintColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1) // title and icon back
            self.navigationController!.navigationBar.titleTextAttributes = [ NSAttributedString.Key.foregroundColor: UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)] // titleBar
            self.navigationController?.navigationBar.barTintColor = colorVerde
            self.navigationItem.title = "Mi perfil"
            
            //self.sideMenuController?.leftViewBackgroundColor = UIColor(red: 1, green: 1, blue: 1.0, alpha: 1)
            
            UIApplication.shared.statusBarStyle = .lightContent
            
        }, completion: nil)
    }
    

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    
        
        UIView.animate(withDuration: 0.2, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
          //self.sideMenuController?.leftViewBackgroundColor = UIColor(red: 1, green: 1, blue: 1.0, alpha: 1)
        }, completion: nil)
        
        
      
    }
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //getListAlumnos()
 
        //llenarObjeto();
        
    }

    
    @IBAction func onNewComplainClick(_ sender: Any) {
        //let viewController = newComplaintViewController
        //openController(controller: viewController)
    }
    
    @IBAction func onMyComplaintsClick(_ sender: Any) {

    }
    

    
    func llenarObjeto()
    {
//        let alumno = ObjetoAlumno(id: "1", nombre: "Josué Gustavo Hernández Villa", asistencia: "true")
//
//        let alumno2 = ObjetoAlumno(id: "2", nombre: "Josué Gustavo Hernández Villa", asistencia: "false")
//
//
//        let alumno3 = ObjetoAlumno(id: "3", nombre: "Josué Gustavo Hernández Villa", asistencia: "false")
//
//        let alumno4 = ObjetoAlumno(id: "4", nombre: "Josué Gustavo Hernández Villa", asistencia: "false")
//
//        ObjetoAlumno.getData().add(alumno)
//        ObjetoAlumno.getData().add(alumno2)
//        ObjetoAlumno.getData().add(alumno3)
//        ObjetoAlumno.getData().add(alumno4)
        
    }
    
    func openLogin(){
        let window = UIApplication.shared.delegate!.window!!
        window.rootViewController = loginViewController
        
        UIView.transition(with: window, duration: 0.3, options: [.transitionCrossDissolve], animations: nil, completion: nil)
    }
    
    @IBAction func onOpenMenuClick(_ sender: UIBarButtonItem) {
        sideMenuController?.showLeftView(animated: true, completionHandler: nil)
    }
    
    func openController(controller: UIViewController){
        let viewController = controller
        
        
        let navigationController = NavigationViewController(rootViewController: viewController)
        
        let mainViewController = menuViewControllerLoaded
        mainViewController.rootViewController = navigationController
        mainViewController.setup(type: 0)
        
        let window = UIApplication.shared.delegate!.window!!
        window.rootViewController = mainViewController
        
        UIView.transition(with: window, duration: 0.3, options: [.transitionCrossDissolve], animations: nil, completion: nil)
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ObjetoAlumno.getData().count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : cellAlumnos = tableView.dequeueReusableCell(withIdentifier: "cellAlumnos", for: indexPath) as! cellAlumnos
        let alumno : ObjetoAlumno = ObjetoAlumno.getData().object(at: indexPath.row) as! ObjetoAlumno
        //cell.textLabel?.text = "index -- \((indexPath as NSIndexPath).row)"
        cell.nombre.text = alumno.nombre
        cell.nombre.adjustsFontSizeToFitWidth = true
        
        if alumno.asistencia == "true"
        {
            cell.status.image = UIImage(named: "asistencia.png")
        }
        else
        {
            cell.status.image = UIImage(named: "falta.png")
        }
        
        cell.button.tag = indexPath.row
        cell.button.addTarget(self, action: #selector(connected(sender:)), for: .touchUpInside)
        
        return cell
    }
    
    @objc func connected(sender: UIButton){
        let buttonTag = sender.tag
        
        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "DetalleAlumno") as? DetalleAlumno
        vc?.positionAlumno = buttonTag
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    
    
    
    
    
    
    func getListAlumnos()
    {
        HUD.show(.progress)
        
        ObjetoAlumno.getData().removeAllObjects()
        let ppl_code_id = String(describing: UserDefaults.standard.value(forKey: DataPersistent.id)!)
        //let idViajePorNotificacion = String(describing: UserDefaults.standard.value(forKey: DataPersistent.id_viaje)!)
        
        
        
//        let parameters: Parameters = [
//            "ac": "2",
//            "id_profesor" : ppl_code_id,
//            "token" : "37P13D6kQn4B430hWJR3"
//        ]
        
        let parameters: Parameters = [
            "ac": "2",
            "id_profesor" : ppl_code_id
        ]
        
        print("\(ApiDefinition.WS_GENERAL)?ac=2&id_profesor=\(ppl_code_id)")
        
        Alamofire.request(ApiDefinition.WS_GENERAL, method: .post, parameters : parameters, encoding: URLEncoding.default).validate().responseJSON { response in
         //Alamofire.request("\(ApiDefinition.WS_GENERAL)?ac=2&id_profesor=\(ppl_code_id)", method: .get, parameters : parameters, encoding: URLEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                
                
                print("WS_GENERAL: \(json)")
                print("WS_GENERAL: \(json.count)")
                //let resultado = json["estado"]
                
                let clase = json["Current_Class"]
                self.status_clase = String(describing: clase["status"])
                self.token_clase = String(describing: clase["token"])
                self.class_name_clase = String(describing: clase["class_name"])
                self.teacher_id_clase = String(describing: clase["teacher_id"])
                self.teacher_name_clase = String(describing: clase["teacher_name"])
                
                print(self.status_clase)
                print(self.teacher_id_clase)
                print(self.class_name_clase)
                print(self.token_clase)
                
                self.headerTitulo.text = self.class_name_clase
                self.headerTitulo.adjustsFontSizeToFitWidth = true
                
                let res = json["Student_List"]
                print("RES: \(res)")
                print("RES count: \(res[0].count)")
                var contador = 0
                
                if (self.token_clase != "null")
                {
                    for obj in res[0]
                    {
                        
                        let id = String(describing: res[0][contador]["id"])
                        let student_id = String(describing: res[0][contador]["student_id"])
                        //let key = String(describing: res[0][contador]["key"])
                        let attendance = String(describing: res[0][contador]["attendance"])
                        let student_name = String(describing: res[0][contador]["student_name"])
                        let rating = String(describing: res[0][contador]["rating"])
                        let balance = String(describing: res[0][contador]["balance"])
                        contador = contador + 1
                       
                        
                        let objeto = ObjetoAlumno(id: id, student_id: student_id, nombre: student_name, asistencia: attendance, balance: balance, rating: rating)
                        ObjetoAlumno.getData().add(objeto)
                    }
                    
                    PKHUD.sharedHUD.hide(afterDelay: 0.5) { success in
                        
                        //self.tableView.reloadData()
                        //                    let alert = UIAlertController(title: "U. Insurgentes", message: "No se ha podido autenticar el usuario", preferredStyle: .alert)
                        //                    let actionOK : UIAlertAction = UIAlertAction(title: "Deacuerdo", style: .default, handler: nil)
                        //                    alert.addAction(actionOK)
                        //                    self.present(alert, animated: true, completion: nil)
                    }
                    
                }
                else
                {
                    PKHUD.sharedHUD.hide(afterDelay: 0.5) { success in
                        let alert = UIAlertController(title: "U. Insurgentes", message: "No tiene una clase disponible en este horario", preferredStyle: .alert)
                        let actionOK : UIAlertAction = UIAlertAction(title: "Deacuerdo", style: .default, handler: nil)
                        alert.addAction(actionOK)
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                
                
              

                
//                if res != "1"
//                {
//
//                    let viaje = json["profesor"]
//
//                    let id2 = String(describing: viaje[0]["id"])
//                    let id_pasajero = String(describing:viaje[0]["id_pasajero"])
//                    let id_conductor = String(describing:viaje[0]["id_conductor"])
//
//                    PKHUD.sharedHUD.hide(afterDelay: 0.5) { success in
//
//
//                    }
//                }
//                else
//                {
//

//                }
            case .failure(let error):
                print(error)
                
                //SwiftSpinner.hide()
                PKHUD.sharedHUD.hide(afterDelay: 0.5) { success in
                let alert = UIAlertController(title: "U. Insurgentes", message: "No se ha podido obtener la lista de alumnos, intente nuevamente", preferredStyle: .alert)
                let actionOK : UIAlertAction = UIAlertAction(title: "Deacuerdo", style: .default, handler: nil)
                alert.addAction(actionOK)
                self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    func sendListaAlumnos(arrayAlumnos : String)
    {
        HUD.show(.progress)
        
        let ppl_code_id = String(describing: UserDefaults.standard.value(forKey: DataPersistent.id)!)
        //let idViajePorNotificacion = String(describing: UserDefaults.standard.value(forKey: DataPersistent.id_viaje)!)
        
        
//        let formatter = DateFormatter()
//        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
//        let now = Date()
//        let dateString = formatter.string(from:now)
//        print(dateString)
        
        //https://syasist.accessmedia.com.mx/ws.php?ac=2&id_profesor=P000008634&token=37P13D6kQn4B430hWJR3
        let parameters: Parameters = [
            "ac": "3",
            "id_profesor" : ppl_code_id,
            "token" : self.token_clase,
            "attendance_list" : arrayAlumnos
        ]
        
        print("\(ApiDefinition.WS_GENERAL)?ac=3&id_profesor=\(ppl_code_id)&token=\(self.token_clase)&attendance_list=\(arrayAlumnos)")
        
        Alamofire.request(ApiDefinition.WS_GENERAL, method: .post, parameters : parameters, encoding: URLEncoding.default).validate().responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                
                print("WS_GENERAL: \(json)")
                //let resultado = json["estado"]
                
        
                //let resultado = json["estado"]
                let res = String(describing: json["status"])
                print("RES \(res)")
                if (res == "1")
                {
                    
//                    let viaje = json["profesor"]
//
//                    let id2 = String(describing: viaje[0]["id"])
//                    let id_pasajero = String(describing:viaje[0]["id_pasajero"])
//                    let id_conductor = String(describing:viaje[0]["id_conductor"])
                    
                    PKHUD.sharedHUD.hide(afterDelay: 0.5) { success in
                        self.status_clase = "1"
                        let alert = UIAlertController(title: "U. Insurgentes", message: "Lista sincronizada con éxito", preferredStyle: .alert)
                        let actionOK : UIAlertAction = UIAlertAction(title: "Deacuerdo", style: .default, handler: nil)
                        alert.addAction(actionOK)
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                else if (res == "2")
                {
                    PKHUD.sharedHUD.hide(afterDelay: 0.5) { success in
                        let alert = UIAlertController(title: "U. Insurgentes", message: "Ya ha enviado la lista de asistencia anteriormente", preferredStyle: .alert)
                        let actionOK : UIAlertAction = UIAlertAction(title: "Deacuerdo", style: .default, handler: nil)
                        alert.addAction(actionOK)
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                    
                else
                {
                    
                    PKHUD.sharedHUD.hide(afterDelay: 0.5) { success in
                        let alert = UIAlertController(title: "U. Insurgentes", message: "No se ha podido sincronizar la lista de alumnos, intente nuevamente", preferredStyle: .alert)
                        let actionOK : UIAlertAction = UIAlertAction(title: "Deacuerdo", style: .default, handler: nil)
                        alert.addAction(actionOK)
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            case .failure(let error):
                print(error)
                
                //SwiftSpinner.hide()
                PKHUD.sharedHUD.hide(afterDelay: 0.5) { success in
                    let alert = UIAlertController(title: "U. Insurgentes", message: "No se ha podido sincronizar la lista de alumnos, intente nuevamente", preferredStyle: .alert)
                    let actionOK : UIAlertAction = UIAlertAction(title: "Deacuerdo", style: .default, handler: nil)
                    alert.addAction(actionOK)
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    
    @IBAction func actionAceptarLista(_ sender: UIButton) {
        
        if (self.token_clase != "null")
        {
            var arrayAlumnos = ""
            
            var contador = 0
            for objetoAlumno in ObjetoAlumno.getData() {
                let alumno :  ObjetoAlumno = objetoAlumno as! ObjetoAlumno
                
                var asistencia_numero = "0"
                
                print(alumno.asistencia)
                if (alumno.asistencia == "true")
                {
                    asistencia_numero = "1"
                }
                else
                {
                    asistencia_numero = "0"
                }
                
                if (contador == ObjetoAlumno.getData().count - 1)
                {
                    arrayAlumnos = arrayAlumnos + alumno.student_id + ":" + asistencia_numero
                }
                else
                {
                    arrayAlumnos = arrayAlumnos + alumno.student_id + ":" + asistencia_numero + ","
                }
                
                contador = contador + 1
            }
            
            print(arrayAlumnos)
            
            sendListaAlumnos(arrayAlumnos: arrayAlumnos)
            
        }
        else
        {
            //PKHUD.sharedHUD.hide(afterDelay: 0.5) { success in
                let alert = UIAlertController(title: "U. Insurgentes", message: "No tiene una clase disponible en este horario", preferredStyle: .alert)
                let actionOK : UIAlertAction = UIAlertAction(title: "Deacuerdo", style: .default, handler: nil)
                alert.addAction(actionOK)
                self.present(alert, animated: true, completion: nil)
            //}
        }
    }
    
    
    
    
    
}


extension UIView {
    func addShadow() {
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 2)
        self.layer.shadowOpacity = 0.2
        self.layer.shadowRadius = 5.0
        self.layer.cornerRadius = self.layer.frame.size.width / 2
        self.clipsToBounds = false
    }
}

