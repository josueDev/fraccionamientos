//
//  LoginViewController.swift
//  x24
//
//  Created by Josué :D on 30/01/18.
//  Copyright © 2018 ivanysusamikos. All rights reserved.
//

import UIKit
import SwiftSpinner
import Alamofire
import SwiftyJSON
import PKHUD


class LoginViewController: UIViewController{
    
    lazy var mainViewControllerLoaded: MainViewController = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        var view = storyboard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
        
        return view
    }()
    
    
    lazy var menuViewControllerLoaded: MenuViewController = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        var view = storyboard.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        
        return view
    }()
    
    lazy var registroViewControllerLoaded: Registro = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        var view = storyboard.instantiateViewController(withIdentifier: "Registro") as! Registro
        
        return view
    }()
    
    
    @IBOutlet weak var userTextField: DesignableUITextField!
    @IBOutlet weak var passTextField: DesignableUITextField!
    @IBOutlet weak var loginButton: UIButton!
    
    @IBOutlet var textFields: [DesignableUITextField]!
    @IBOutlet var buttonIniciaSesion: UIButton!
    
    override func viewWillAppear(_ animated: Bool) {

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
     
        buttonIniciaSesion.isUserInteractionEnabled = false;
        updateView()
        
        //userTextField.placeholder = "Usuario"
       userTextField.text = "demo@am.com"
        passTextField.text = "123456"
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onLoginClick(_ sender: UIButton) {
        
        //loadHome()
        
        
         //getLoginFake()
        
//        if isValid(){
//
//            //loadHome()
//            //UserDefaults.standard.setValue("true", forKey: DataPersistent.TERMINAL_LOGIN)
//            //UserDefaults.standard.synchronize()
//
//            
            SwiftSpinner.show("Iniciando sesión", animated: true)
            getLogin()
//
//
//
//
//
//            //loginPresenter?.login(ac : "1",email: userTextField.text!, password: passTextField.text!) //1 = LOGIN
//
////            if isValidEmail(testStr: userTextField.text!){
////
////            }else{
////                CustomDialog.showDialog(controller: self, title: "", message: "Correo electronico no válido, intente con otra cuenta")
////            }
//
//        }else{
//            CustomDialog.showAcceptDialgo(controller: self, title: "", message: "Debe ingresar usuario y contraseña para iniciar sesión") { () -> Void in
//                self.shakeFields()
//            }
//
//        }
        
    }
    
    @IBAction func onOpenMenuClick(_ sender: UIBarButtonItem) {
        sideMenuController?.showLeftView(animated: true, completionHandler: nil)
    }
    
     func openMenu() {
        sideMenuController?.showLeftView(animated: true, completionHandler: nil)
    }


    func updateView() {
        //userTextField.text = loginPresenter!.getUser()
        loginButton.addShadowColor(color: "#00416a", width: -2, height: 4)
        userTextField.addShadowColor(color: "#3C3C3C", width: 1, height: 1)
        passTextField.addShadowColor(color: "#3C3C3C", width: 1, height: 1)
    }

    func isValid()-> Bool{
        var isValid = true
        
        
        for textField in textFields{
            if (textField.text?.isEmpty)! {
                isValid = false
                textField.addShadowColor(color: "#AB3C3F", width: 1, height: 1)
            }else{
                textField.addShadowColor(color: "#3C3C3C", width: 1, height: 1)
            }
        }
        
        
        return isValid
    }
    

    func recoverPassword(email: String){
        if email.isEmpty{
            CustomDialog.showDialog(controller: self, title: "", message: "Debe capturar un correo electrónico")
        }else{
            SwiftSpinner.show("Enviando información")
            //loginPresenter!.recoveryPassword(email: email)
        }
    }
    
    func onErrorRecoveryPassword(message: String) {
        SwiftSpinner.hide()
        CustomDialog.showDialog(controller: self, title: "Recuperar contraseña", message: message)
    }
    
    func onSuccessRecoveryPassword(message: String) {
        SwiftSpinner.hide()
        CustomDialog.showDialog(controller: self, title: "Recuperar contraseña", message: message)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "Register") {
            //let viewController = segue.destination as! RegisterViewController
            // Now you have a pointer to the child view controller.
            // You can save the reference to it, or pass data to it.
            
        }
    }
    
   
    @IBAction func onForgotPasswordClick(_ sender: Any) {
        let alert = UIAlertController(title: "¿Olvidase tu contraseña?", message: "Ingrese el correo con el que se registró", preferredStyle: .alert)
        
        //2. Add the text field. You can configure it however you need.
        alert.addTextField { (textField) in
            textField.placeholder = "Email"
        }
        
        // 3. Grab the value from the text field, and print it when the user clicks OK.
        alert.addAction(UIAlertAction(title: "Aceptar", style: .default, handler: { [weak alert] (_) in
            let textField = alert!.textFields![0] // Force unwrapping because we know it exists.
            self.recoverPassword(email: textField.text!)
        }))
        
        // 3. Grab the value from the text field, and print it when the user clicks OK.
        alert.addAction(UIAlertAction(title: "Canceler", style: .destructive , handler:nil))
        
        // 4. Present the alert.
        self.present(alert, animated: true, completion: nil)
    }
    
    func onErrorLogin(message: String) {
        SwiftSpinner.hide()
        CustomDialog.showDialog(controller: self, title: "", message: message)
    }
    
    func shakeFields(){
        for textField in textFields{
            if ((textField.text?.isEmpty)!){
                textField.shake()
            }
        }
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "^(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?(?:(?:(?:[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+(?:\\.[-A-Za-z0-9!#$%&’*+/=?^_'{|}~]+)*)|(?:\"(?:(?:(?:(?: )*(?:(?:[!#-Z^-~]|\\[|\\])|(?:\\\\(?:\\t|[ -~]))))+(?: )*)|(?: )+)\"))(?:@)(?:(?:(?:[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)(?:\\.[A-Za-z0-9](?:[-A-Za-z0-9]{0,61}[A-Za-z0-9])?)*)|(?:\\[(?:(?:(?:(?:(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))\\.){3}(?:[0-9]|(?:[1-9][0-9])|(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5]))))|(?:(?:(?: )*[!-Z^-~])*(?: )*)|(?:[Vv][0-9A-Fa-f]+\\.[-A-Za-z0-9._~!$&'()*+,;=:]+))\\])))(?:(?:(?:(?: )*(?:(?:(?:\\t| )*\\r\\n)?(?:\\t| )+))+(?: )*)|(?: )+)?$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: testStr)
        return result
    }
    
    func loadHome()
    {
        let viewController = mainViewControllerLoaded

        let navigationController = NavigationViewController(rootViewController: viewController)

        let mainViewController = menuViewControllerLoaded
        mainViewController.rootViewController = navigationController
        mainViewController.setup(type: 0)

        let window = UIApplication.shared.delegate!.window!!
        window.rootViewController = mainViewController

        UIView.transition(with: window, duration: 0.3, options: [.transitionCrossDissolve], animations: nil, completion: nil)
    }
    
    func getLoginFake()
    {
        //let vc = Registro() //change this to your class name
        //self.present(vc, animated: true, completion: nil)
        
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let vc = storyboard.instantiateViewController(withIdentifier: "Registro") as! Registro
//        //vc.newsObj = newsObj
//        navigationController?.pushViewController(vc,animated: true)
        
//        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
////

        
        
        
        
        
        
        let viewController = registroViewControllerLoaded

        let navigationController = NavigationViewController(rootViewController: viewController)

        let mainViewController = registroViewControllerLoaded
        //mainViewController.rootViewController = navigationController
        //mainViewController.setup(type: 0)

        let window = UIApplication.shared.delegate!.window!!
        window.rootViewController = mainViewController

        UIView.transition(with: window, duration: 0.3, options: [.transitionCrossDissolve], animations: nil, completion: nil)
        
        

    }
    
    @objc func getLogin()
    {
        //HUD.show(.progress)

        
        //let id = String(describing: UserDefaults.standard.value(forKey: DataPersistent.id)!)
        //let idViajePorNotificacion = String(describing: UserDefaults.standard.value(forKey: DataPersistent.id_viaje)!)
        
        let parameters: Parameters = [
            "ac": "1",
            "UsrName" : userTextField.text!,
            "UsrPass" : passTextField.text!
           
        ]
        
        print("\(ApiDefinition.WS_GENERAL)?ac=1&UsrName=\(userTextField.text!)&UsrPass=\(passTextField.text!)")
         Alamofire.request(ApiDefinition.WS_GENERAL, method: .post, parameters : parameters, encoding: URLEncoding.default).validate().responseJSON { response in
            
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                
                
                SwiftSpinner.hide()
                print("WS_GENERAL: \(json)")
                //let resultado = json["estado"]
                let res = String(describing: json["estado"])
                print("RES \(res)")
                if ((res) == "1")
                {
                    
                    let usuario = json["Usuario"]
                    
                    let id = String(describing: usuario["id"])
                    let token = String(describing:usuario["token"])
                    let rol = String(describing:usuario["rol"])
                    let name = String(describing: usuario["name"])
                    let last_name = String(describing:usuario["last_name"])
                    let email = String(describing:usuario["email"])
                    let phone = String(describing: usuario["phone"])
                    let cel_phone = String(describing:usuario["cel_phone"])
                    let home_location = String(describing:usuario["home_location"])
                    let number = String(describing: usuario["number"])
                    let custom_payment = String(describing:usuario["custom_payment"])
                    let status = String(describing:usuario["status"])
                    
                   
                    UserDefaults.standard.setValue("true", forKey: DataPersistent.TERMINAL_LOGIN)
                    
                    UserDefaults.standard.setValue(id, forKey: DataPersistent.id)
                    UserDefaults.standard.setValue(token, forKey: DataPersistent.token)
                    UserDefaults.standard.setValue(rol, forKey: DataPersistent.rol)
                    UserDefaults.standard.setValue(last_name, forKey: DataPersistent.last_name)
                    UserDefaults.standard.setValue(email, forKey: DataPersistent.email)
                    UserDefaults.standard.setValue(phone, forKey: DataPersistent.phone)
                    UserDefaults.standard.setValue(cel_phone, forKey: DataPersistent.cel_phone)
                    UserDefaults.standard.setValue(home_location, forKey: DataPersistent.home_location)
                    UserDefaults.standard.setValue(number, forKey: DataPersistent.number)
                    UserDefaults.standard.setValue(custom_payment, forKey: DataPersistent.custom_payment)
                    UserDefaults.standard.setValue(status, forKey: DataPersistent.status)
                    UserDefaults.standard.synchronize()

                    self.loadHome()
                    //PKHUD.sharedHUD.hide(afterDelay: 0.5) { success in
                        
                        
                    //}
                }
                else
                {

                    //PKHUD.sharedHUD.hide(afterDelay: 0.5) { success in
                        let alert = UIAlertController(title: "U. Insurgentes", message: "No se ha podido autenticar el usuario", preferredStyle: .alert)
                        let actionOK : UIAlertAction = UIAlertAction(title: "Deacuerdo", style: .default, handler: nil)
                        alert.addAction(actionOK)
                        self.present(alert, animated: true, completion: nil)
                    //}
                }
            case .failure(let error):
                print(error)

            SwiftSpinner.hide()
                //PKHUD.sharedHUD.hide(afterDelay: 0.5) { success in
                    let alert = UIAlertController(title: "U. Insurgentes", message: "No se ha podido autenticar el usuario", preferredStyle: .alert)
                    let actionOK : UIAlertAction = UIAlertAction(title: "Deacuerdo", style: .default, handler: nil)
                    alert.addAction(actionOK)
                    self.present(alert, animated: true, completion: nil)
                //}
            }
        }
    }
    
    @IBAction func actionRegistro(_ sender: UIButton) {
        
        let viewController = registroViewControllerLoaded
        
        let navigationController = NavigationViewController(rootViewController: viewController)
        
        let mainViewController = registroViewControllerLoaded
        //mainViewController.rootViewController = navigationController
        //mainViewController.setup(type: 0)
        
        let window = UIApplication.shared.delegate!.window!!
        window.rootViewController = mainViewController
        
        UIView.transition(with: window, duration: 0.3, options: [.transitionCrossDissolve], animations: nil, completion: nil)
        
    }
    
}

extension UIView {
    func addShadowColor(color: String, width: Int, height: Int) {
        self.layer.masksToBounds = false
        self.layer.shadowRadius = 4
        self.layer.shadowOpacity = 1
        self.layer.shadowColor = UIColor(hexString: color)?.withAlphaComponent(0.4).cgColor
        self.layer.shadowOffset = CGSize(width: width , height:height)
    }
}

extension UIColor {
    convenience init?(hexString: String) {
        var chars = Array(hexString.hasPrefix("#") ? hexString.dropFirst() : hexString[...])
        let red, green, blue, alpha: CGFloat
        switch chars.count {
        case 3:
            chars = chars.flatMap { [$0, $0] }
            fallthrough
        case 6:
            chars = ["F","F"] + chars
            fallthrough
        case 8:
            alpha = CGFloat(strtoul(String(chars[0...1]), nil, 16)) / 255
            red   = CGFloat(strtoul(String(chars[2...3]), nil, 16)) / 255
            green = CGFloat(strtoul(String(chars[4...5]), nil, 16)) / 255
            blue  = CGFloat(strtoul(String(chars[6...7]), nil, 16)) / 255
        default:
            return nil
        }
        self.init(red: red, green: green, blue:  blue, alpha: alpha)
    }
}

extension UIView {
    func shake() {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        animation.duration = 0.6
        animation.values = [-20.0, 20.0, -20.0, 20.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
        layer.add(animation, forKey: "shake")
    }
    
    
}
