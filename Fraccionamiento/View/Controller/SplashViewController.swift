//
//  SplashViewController.swift
//  atiende
//
//  Created by Christian Vargas Saavedra on 10/23/18.
//  Copyright © 2018 Christian Vargas Saavedra. All rights reserved.
//

import UIKit
import Lottie

class SplashViewController: UIViewController{
    
    private var loaderAnimation: LOTAnimationView?

    
    lazy var loginViewControllerLoaded: LoginViewController = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        var view = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        
        return view
    }()
    
    
    lazy var menuViewControllerLoaded: MenuViewController = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        var view = storyboard.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        
        return view
    }()
    
    lazy var mainViewControllerLoaded: MainViewController = {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        
        var view = storyboard.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
        
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //updateView()

        //splashPresenter = SplashPresenter(delegate: self)
        
        let login = String(describing: UserDefaults.standard.value(forKey: DataPersistent.TERMINAL_LOGIN)!)
        
        if login == "true"
        {
            loadHome()
        }
        else
        {
            loadLogin()
        }
        
        
    }
    
    
    
    func updateView(){
        // Create Boat Animation
        loaderAnimation = LOTAnimationView(name: "animationLoaderSplash.json")
        // Set view to full screen, aspectFill
        loaderAnimation!.frame = CGRect(x:view.frame.minX - 50, y: view.frame.maxY - 285, width: view.frame.width + 100, height: 470)

        loaderAnimation!.loopAnimation = true
        
        // Add the Animation
        view.addSubview(loaderAnimation!)
        
        loaderAnimation?.play()
    }
    
    func loadHome()
    {
        let viewController = mainViewControllerLoaded
        
        let navigationController = NavigationViewController(rootViewController: viewController)
        navigationController.navigationBar.tintColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1) // title and icon back
        sideMenuController?.leftViewBackgroundColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1) // title and icon back
        
        let mainViewController = menuViewControllerLoaded
        mainViewController.rootViewController = navigationController
        mainViewController.setup(type: 0)
        
        let window = UIApplication.shared.delegate!.window!!
        window.rootViewController = mainViewController
        
        UIView.transition(with: window, duration: 0.3, options: [.transitionCrossDissolve], animations: nil, completion: nil)
    }
    
    func loadLogin()
    {
        let viewController = loginViewControllerLoaded
        
        let navigationController = NavigationViewController(rootViewController: viewController)
        
        let mainViewController = loginViewControllerLoaded
        //mainViewController.rootViewController = navigationController
        //mainViewController.setup(type: 0)
        
        let window = UIApplication.shared.delegate!.window!!
        window.rootViewController = mainViewController
        
        UIView.transition(with: window, duration: 0.3, options: [.transitionCrossDissolve], animations: nil, completion: nil)
    }
    
    
    
    func onSuccessLoadCatalogs() {

    }
    
    func onErrorLoadCatalogs() {
       CustomDialog.showDialog(controller: self, title: "", message: "No se pudo obtener la información requerida para iniciar")
    }
    
    func onErrorConnection() {
         CustomDialog.showDialog(controller: self, title: Keys.CONNECTION_TITLE , message: Keys.CONNECTION_MESSAGE)
    }
    
    
}
