//
//  CustomDialog.swift
//  atiende
//
//  Created by Christian Vargas Saavedra on 10/23/18.
//  Copyright © 2018 Christian Vargas Saavedra. All rights reserved.
//

import UIKit

public class CustomDialog{
    typealias action = ()->Void
    static func showAcceptDialgo(controller: UIViewController , title: String, message: String, acceptAction:@escaping action ){
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: {
            (action) -> Void in
            acceptAction()
        }))

        controller.present(alert, animated: true, completion: nil)
    }
    
    static func showAcceptCancelDialgo2(controller: UIViewController , title: String, message: String, acceptAction:@escaping action,  cancelAction:@escaping action ){
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: {
            (action) -> Void in
            acceptAction()
        }))
        
        controller.present(alert, animated: true, completion: cancelAction)
    }
    
    static func showAcceptCancelDialgo(controller: UIViewController , title: String, message: String, acceptAction:@escaping ()->Void){
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: {
            (action) -> Void in
            acceptAction()
        }))
        
        alert.addAction(UIAlertAction(title: "Cancelar", style: UIAlertAction.Style.destructive, handler: nil))
        
        controller.present(alert, animated: true, completion: nil)
    }
    
    static func showDialog(controller: UIViewController , title: String, message: String){
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil))
        
        controller.present(alert, animated: true, completion: nil)
    }
}
