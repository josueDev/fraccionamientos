//
//  String+Ext.swift
//  informacndh
//
//  Created by Lennken Group on 9/19/18.
//  Copyright © 2018 Lennken Group. All rights reserved.
//

import Foundation
import UIKit

extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [kCTFontAttributeName as NSAttributedString.Key: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [kCTFontAttributeName as NSAttributedString.Key: font], context: nil)
        
        return ceil(boundingBox.width)
    }
    var htmlToAttributedString: NSMutableAttributedString? {
        guard let data = data(using: .utf8) else { return NSMutableAttributedString() }
        do {
            return try NSMutableAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSMutableAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
    
    func searchText(text: String, attributes: [NSAttributedString.Key: Any]) -> NSMutableAttributedString {
        
        let searchString = text.folding(options: .diacriticInsensitive, locale: .current)
        let conAcentos = self
        let attributed = NSMutableAttributedString(string: conAcentos)
        let baseString = attributed.string.folding(options: .diacriticInsensitive, locale: .current)
        attributed.addAttributes(attributes, range: NSRange(location: 0, length: attributed.length))
        if text.isEmpty {
            return attributed
        }
        guard let regex = try?
            NSRegularExpression(pattern: searchString, options: NSRegularExpression.Options.caseInsensitive
            ) else {
                return attributed
        }
        var count = 0
        for match in regex.matches(in: baseString, options: NSRegularExpression.MatchingOptions.withTransparentBounds, range: NSRange(location: 0, length: baseString.utf16.count)) as [NSTextCheckingResult] {
            count += 1
            attributed.addAttribute(NSAttributedString.Key.backgroundColor, value: UIColor(hex: "FEFB01"), range: match.range)
        }
        return attributed
    }
    
}
