//
//  UITextField+Designable.swift
//  informacndh
//
//  Created by Lennken Group on 9/19/18.
//  Copyright © 2018 Lennken Group. All rights reserved.
//

import Foundation
import UIKit

/// Texfield personalizado
@IBDesignable
class UITextFieldX: UITextField {
    
    /// Imageview para el lado izquierdo del texfield
    var leftImageView: UIImageView!
    /// Imageview para el lado derecho del texfield
    var imageView = UIImageView(frame: CGRect(x: 5, y: 0, width: 20, height: 20))
    
    /// Imagen del lado izquierdo
    @IBInspectable var leftImage: UIImage? {
        didSet {
            updateView()
        }
    }
    
    /// Pading para el lado izquiero
    @IBInspectable var leftPadding: CGFloat = 0 {
        didSet {
            updateView()
        }
    }
    
    /// Imagen para el lado derecho
    @IBInspectable var rightImage: UIImage? {
        didSet {
            if let img = rightView?.subviews.first as? UIImageView {
                img.image = rightImage
                self.layoutIfNeeded()
            }else{
                updateView()
            }
        }
    }
    
    /// Pading para el lado derecho
    @IBInspectable var rightPadding: CGFloat = 0 {
        didSet {
            updateView()
        }
    }
    
    private var _isRightViewVisible: Bool = true
    
    var isRightViewVisible: Bool {
        get {
            return _isRightViewVisible
        }
        set {
            _isRightViewVisible = newValue
            updateView()
        }
    }
    
    /// Dibuja bordes en los lados que se le especifica
    ///
    /// - Parameters:
    ///   - sides: Arreglo que contiene ViewSide
    ///   - thickness: Ancho de la linea del borde
    ///   - color: Color para pintar la linea del borde
    func borderLeftImage(_ sides: ViewSide..., thickness: CGFloat = 0.0, color: UIColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)) -> Void {
        leftView?.frame.size.height = screen.height * 0.08
        leftView?.frame.size.width = screen.width * 0.16
        
        leftImageView.center = (leftView?.center)!
        
        
        sides.forEach { (side) in
             self.leftView?.addBorder(toSide: side, withColor: color.cgColor, andThickness: thickness)
        }
       
    }
    
    /// Actualiza vista
    func updateView() {
        setLeftImage()
        setRightImage()
        
        // Placeholder text color
    }
    
    /// Establece imagen en el lado izquierdo
    func setLeftImage() {
        leftViewMode = UITextField.ViewMode.always
        var view: UIView
        
        if leftImageView == nil {
            leftImageView = UIImageView(frame: CGRect(x: 5, y: 0, width: 20, height: 20))
        }
        
       
        if let image = leftImage {
            
            leftImageView.image = image
            // Note: In order for your image to use the tint color, you have to select the image in the Assets.xcassets and change the "Render As" property to "Template Image".
            leftImageView.tintColor = tintColor
            leftImageView.contentMode = .scaleAspectFit
            var width = leftImageView.frame.width + leftPadding
            
            if borderStyle == UITextField.BorderStyle.none || borderStyle == UITextField.BorderStyle.line {
                width += 5
            }
            
            view = UIView(frame: CGRect(x: 0, y: 0, width: width, height: leftImageView.frame.height))
            view.addSubview(leftImageView)
        } else {
            view = UIView(frame: CGRect(x: 0, y: 0, width: leftPadding, height: leftImageView.frame.height))
        }
        
        leftView = view
    }
    /// Establece imagen en el lado derecho
    func setRightImage() {
        rightViewMode = UITextField.ViewMode.always
        
        var view: UIView
        
        if let image = rightImage, isRightViewVisible {
            
            imageView.image = image
            // Note: In order for your image to use the tint color, you have to select the image in the Assets.xcassets and change the "Render As" property to "Template Image".
            imageView.tintColor = tintColor
            imageView.contentMode = .scaleAspectFit
            var width = imageView.frame.width + rightPadding + 5 
            
            if borderStyle == UITextField.BorderStyle.none || borderStyle == UITextField.BorderStyle.line {
                width += 5
            }
            
            view = UIView(frame: CGRect(x: 0, y: 0, width: width, height: 20))
            view.addSubview(imageView)
            
        } else {
            view = UIView(frame: CGRect(x: 0, y: 0, width: rightPadding, height: 20))
        }
        
        rightView = view
    }
    
    var padding = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        if leftImageView != nil {
            padding.left = (leftView?.frame.width)! + 5
        }
        if rightView != nil {
            padding.right = (rightView?.frame.width)! + 3
        }
        return bounds.inset(by: padding)
    }
    
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }


    // MARK: - Corner Radius
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            self.clipsToBounds = true
            self.layer.cornerRadius = cornerRadius
        }
    }
}
