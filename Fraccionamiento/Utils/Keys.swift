//
//  Keys.swift
//  atiende
//
//  Created by Christian Vargas Saavedra on 10/17/18.
//  Copyright © 2018 Christian Vargas Saavedra. All rights reserved.
//

import Foundation

class Keys {
    
    static let SOURCE_LOGIN = "mobile"
    static let GRANT_TYPE = "password"
    static let PUSH_ID = "0"
    
    static let SYSTEM_INFO = "{'iOSVersion':'12.01','AppVersion':'1.0','Signal':'Excelente','apiLevel':'22','architecture':'armv7l','availableStorage':'1218','brand':'motorola','dimensions':'7201184','internalStorage':'6613','kernelArchitecture':'armv7l','levelBattery':'0.48','model':'XT1032falcon_umts','screenSize':'LPB23.13-56','totalRam':'931999744'}"
    
    static let TELEPHONE = "0"
    static let LATITUDE = "0"
    static let LONGITUDE = "0"
    
    static let KEY_64 = "keyBase64"

    static let CONNECTION_TITLE = "Conexión"
    static let CONNECTION_MESSAGE = "Por favor revise su conexión a internet"

}
