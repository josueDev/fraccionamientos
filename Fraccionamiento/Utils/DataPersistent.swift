//
//  DataPersistent.swift
//  x24
//
//  Created by Josué :D on 27/02/18.
//  Copyright © 2018 ivanysusamikos. All rights reserved.
//

import Foundation

class DataPersistent: NSObject {
    
    static let TERMINAL_INITIALIZE = "TERMINAL_INITIALIZE"
    static let TERMINAL_LOGIN = "TERMINAL_LOGIN"
    
    
    static let id = "id"
    static let token = "token"
    static let rol = "rol"
    static let name = "name"
    static let last_name = "last_name"
    static let email = "email"
    static let phone = "phone"
    static let cel_phone = "cel_phone"
    static let home_location = "home_location"
    static let number = "number"
    static let custom_payment = "custom_payment"
    static let status = "status"

    public func initializeDataPersistent()
    {
        let result = String(describing: UserDefaults.standard.value(forKey: DataPersistent.TERMINAL_INITIALIZE))
        print(result)
        if result == "nil"
        {
            
            UserDefaults.standard.setValue("true", forKey: DataPersistent.TERMINAL_INITIALIZE)
            UserDefaults.standard.setValue("false", forKey: DataPersistent.TERMINAL_LOGIN)

            
            UserDefaults.standard.setValue("", forKey: DataPersistent.id)
            UserDefaults.standard.setValue("", forKey: DataPersistent.token)
            UserDefaults.standard.setValue("", forKey: DataPersistent.rol)
            UserDefaults.standard.setValue("", forKey: DataPersistent.name)
            UserDefaults.standard.setValue("", forKey: DataPersistent.last_name)
            UserDefaults.standard.setValue("", forKey: DataPersistent.email)
            UserDefaults.standard.setValue("", forKey: DataPersistent.phone)
            UserDefaults.standard.setValue("", forKey: DataPersistent.cel_phone)
            UserDefaults.standard.setValue("", forKey: DataPersistent.home_location)
            UserDefaults.standard.setValue("", forKey: DataPersistent.number)
            UserDefaults.standard.setValue("", forKey: DataPersistent.custom_payment)
            UserDefaults.standard.setValue("", forKey: DataPersistent.status)

            
        }
    }
}



//"token": "398e92b9bf4903abd27f3c6d0bbca82d5b26b64e",
//"username": "test",
//"email": "test@test.com",
//"id": "eea4d53d-1003-4337-b2d7-5a51f4a17259",
//"thumbnail": "/media/CACHE/images/users/test/3a764b160b52ed5cb8a829dc64ac1bd8.jpg",
//"first_name": "test",
//"last_name": "test test"

