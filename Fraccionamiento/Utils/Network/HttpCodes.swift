//
//  HttpCodes.swift
//  atiende
//
//  Created by Christian Vargas on 10/16/18.
//  Copyright © 2018 Christian Vargas Saavedra. All rights reserved.
//


import UIKit

class HttpCodes {
    static let SUCCESS_CODE = 200
    static let UNAUTHORIZED = 401
    static let SERVER_ERROR = 500
    static let INCORRECT_CREDENTIALS =  403
    static let INVALID_GRANT = 400
    static let ERROR_CONFLICT = 409
}
