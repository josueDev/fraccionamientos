//
//  Constants.swift
//  informacndh
//
//  Created by Lennken Group on 9/17/18.
//  Copyright © 2018 Lennken Group. All rights reserved.
//

import Foundation
import UIKit

struct K {
    struct Screen {
        static let size = UIScreen.main.bounds.size
        static let height = screen.size.height
        static let width = screen.size.width
        
        static func fwidth() -> CGFloat{
            return  UIScreen.main.bounds.width
        }
        
        static func fheight() -> CGFloat{
            return  UIScreen.main.bounds.height
        }
    }
    
    struct Sizes {
        static var menuSideSize: CGFloat {
            var lines = 2
            if UIDevice().userInterfaceIdiom == .pad {
                lines = 3
            }
            return screen.width/CGFloat(lines)
        }
        
        static var minSpace: CGFloat {
            return screen.height * 0.01
        }
        
        static var normalSpace: CGFloat {
            return screen.height * 0.02
        }
        
        static var xnormalSpace: CGFloat {
            return screen.height * 0.025
        }
        
        static var largeSpace: CGFloat {
            return screen.height * 0.03
        }
    }
    
    struct Fonts {
        struct Header {
            static var minTitle: UIFont {
                return UIDevice().userInterfaceIdiom == .pad ? UIFont.preferredFont(forTextStyle: .title2) : UIFont.preferredFont(forTextStyle: .headline)
            }
            
            static var title: UIFont {
                
                if #available(iOS 11.0, *) {
                    let large = UIFont.boldSystemFont(ofSize: UIFont.preferredFont(forTextStyle: .largeTitle).pointSize)
                    let title = UIFont.boldSystemFont(ofSize: UIFont.preferredFont(forTextStyle: .title2).pointSize)
                    return UIDevice().userInterfaceIdiom == .pad
                        ? devices.isLandscape()
                            ? large.withSize(large.pointSize * 1.5)
                            : large
                        : devices.isLandscape()
                            ? title.withSize(title.pointSize * 1.5)
                            : title
                } else {
                    // Fallback on earlier versions
                    return UIDevice().userInterfaceIdiom == .pad ? UIFont.preferredFont(forTextStyle: .title1) : UIFont.preferredFont(forTextStyle: .title3)
                }
            }
            
        }
        
        struct Size {
            static func body() -> UIFont {
                let body = UIFont.preferredFont(forTextStyle: .body)
                
                return UIDevice.current.userInterfaceIdiom == .pad
                    ? devices.isLandscape()
                        ? body.withSize(body.pointSize * 2.3)
                        : body.withSize(body.pointSize * 1.5)
                    : devices.isLandscape()
                        ? body.withSize(body.pointSize * 1.3)
                        : body
            }
            static func mintitle() -> UIFont {
                
                if #available(iOS 11.0, *) {
                    let large = UIFont.boldSystemFont(ofSize: UIFont.preferredFont(forTextStyle: .title2).pointSize)
                    let title =  UIFont.boldSystemFont(ofSize: UIFont.preferredFont(forTextStyle: .title3).pointSize)  
                    return UIDevice().userInterfaceIdiom == .pad
                        ? devices.isLandscape()
                            ? large.withSize(large.pointSize * 1.8)
                            : large.withSize(large.pointSize * 1.3)
                        : devices.isLandscape()
                        ? title.withSize(title.pointSize * 1.3)
                        : title
                } else {
                    // Fallback on earlier versions
                    return UIDevice().userInterfaceIdiom == .pad ? UIFont.preferredFont(forTextStyle: .title2) : UIFont.preferredFont(forTextStyle: .title3)
                }
            }
            
        }
        
    }
    
    struct Device {
        static var isPhoneX: Bool {
            if UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone, UIScreen.main.nativeBounds.height >= 2436  ||  UIScreen.main.nativeBounds.height == 1792 {
                return true
            }
            return false
        }
        
        static func isLandscape() -> Bool {
           return UIScreen.main.bounds.height < UIScreen.main.bounds.width
        }
    }
    
    struct Colors {
        static let background = #colorLiteral(red: 0.9803921569, green: 0.9803921569, blue: 0.9803921569, alpha: 1)
        static let primaryColor = #colorLiteral(red: 0, green: 0.2549019608, blue: 0.4156862745, alpha: 1)
        static let text = #colorLiteral(red: 0.4274509804, green: 0.431372549, blue: 0.4431372549, alpha: 1)
    }
    
    struct Constraints {
        static var top: CGFloat {
            if UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone, UIScreen.main.nativeBounds.height >= 2436 {
                return 24.0
            }
            return 0.0
        }
    }
}

typealias screen = K.Screen
typealias sizes = K.Sizes
typealias fonts = K.Fonts
typealias devices = K.Device
typealias const = K.Constraints
