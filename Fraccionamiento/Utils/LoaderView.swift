//
//  LoaderView.swift
//  atiende
//
//  Created by Christian Vargas on 10/28/18.
//  Copyright © 2018 Christian Vargas Saavedra. All rights reserved.
//

import UIKit
import Lottie

public class LoaderView{
    
    static func load(view: UIView, title: String){
        
        let w = UIScreen.main.bounds.width
        let h = UIScreen.main.bounds.height

        print(w)
        print(h)
        
        let blurEffect: UIBlurEffect = UIBlurEffect(style: .regular)
        let blurView: UIVisualEffectView = UIVisualEffectView(effect: blurEffect)
        blurView.translatesAutoresizingMaskIntoConstraints = false
        blurView.frame = view.frame

     
        
        let label = UILabel()
        label.text = title
        label.textColor = UIColor(hexString: "#616161")
        label.font = UIFont(name:"HelveticaNeue-Bold", size: 20.0)
        label.sizeToFit()
        
        label.center = view.center

        let loaderAnimation = LOTAnimationView(name: "animationLoaderWebService2.json")
        loaderAnimation.frame = CGRect(x: label.frame.midX - 60, y: label.frame.maxY + 280, width: 120, height: 120)
        loaderAnimation.loopAnimation = true
        
        view.addSubview(blurView)
        view.addSubview(loaderAnimation)
        view.addSubview(label)
 
        loaderAnimation.play()
    }
}
