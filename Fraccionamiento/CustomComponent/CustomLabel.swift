//
//  CustomLabel.swift
//  x24
//
//  Created by Josué :D on 15/02/18.
//  Copyright © 2018 ivanysusamikos. All rights reserved.
//

import UIKit

@IBDesignable class CustomLabel: UILabel {
    
    
    @IBInspectable var topInset: CGFloat = 5.0
    @IBInspectable var bottomInset: CGFloat = 5.0
    @IBInspectable var leftInset: CGFloat = 7.0
    @IBInspectable var rightInset: CGFloat = 7.0
    
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets.init(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        super.drawText(in: rect.inset(by: insets))
        
    }
    
    override var intrinsicContentSize: CGSize {
        let size = super.intrinsicContentSize
        return CGSize(width: size.width + leftInset + rightInset,
                      height: size.height + topInset + bottomInset)
    }

    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    

    
    //    @IBInspectable var cornerRadiusCustom: Double = 5 {
    //        didSet {
    //            self.layer.cornerRadius = CGFloat(cornerRadiusCustom)
    //        }
    //    }
    //
    //    func setup() {
    //        //self.backgroundColor = backColor
    //        //setValue(placeHolderColor, forKeyPath: "_placeholderLabel.textColor")
    //        //textColor = textColorDefault
    //        self.layer.masksToBounds = true
    //        self.layer.cornerRadius = CGFloat(cornerRadiusCustom)
    //    }
    //    override func awakeFromNib() {
    //        setup()
    //    }
    //
    //    override func prepareForInterfaceBuilder() {
    //        setup()
    //    }
    
}



extension UILabel
{
    func drawLine( width1 : CGFloat , height1 : CGFloat,color : UIColor, label : UILabel , size : CGFloat , xSeparation : CGFloat)
    {
        let line = UIView(frame: CGRect(x: 0, y: height1 , width: width1 - xSeparation, height: size))
        line.backgroundColor = color
        label.addSubview(line)
    }
}



