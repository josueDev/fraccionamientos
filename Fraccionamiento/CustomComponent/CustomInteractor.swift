//
//  CustomInteractor.swift
//  atiende
//
//  Created by Christian Vargas on 11/25/18.
//  Copyright © 2018 Christian Vargas Saavedra. All rights reserved.
//

import UIKit

class CustomInteractor : UIPercentDrivenInteractiveTransition {
    
    var navigationController : UINavigationController
    var shouldCompleteTransition = false
    var transitionInProgress = false
    
    init?(attachTo viewController : UIViewController) {
        if let nav = viewController.navigationController {
            self.navigationController = nav
            super.init()
        } else {
            return nil
        }
    }
}
