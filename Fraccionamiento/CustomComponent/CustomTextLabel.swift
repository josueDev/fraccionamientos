//
//  CustomTextLabel.swift
//  atiende
//
//  Created by Josué :D on 06/10/18.
//  Copyright © 2018 Christian Vargas Saavedra. All rights reserved.
//

import UIKit

/// Custom TextField
@IBDesignable class CustomTextLabel: UITextField {
    
    /// Creating UIImageView as leftside of UITextField and Setting image for UIImageView
    @IBInspectable var leftSide:UIImage {
        get {
            return UIImage()
        } set {
            let left: UIImageView = UIImageView(frame: CGRect(x: 0.0, y: 0.0, width: 40.0, height: self.frame.size.height))
            left.image = newValue
            left.contentMode = .center
            
            leftViewMode = .always
            self.leftView = left
        }
    }
    
    
    @IBInspectable var backColor: UIColor = UIColor.lightGray {
        didSet {
            self.backgroundColor = backColor
        }
    }
    
    @IBInspectable var placeHolderColor : UIColor = UIColor(red: 30/255, green: 30/255, blue: 30/255, alpha: 1.00){
        didSet {
            setValue(placeHolderColor, forKeyPath: "_placeholderLabel.textColor")
        }
    }
    
    @IBInspectable var textColorDefault : UIColor = UIColor(red: 1/255, green: 1/255, blue: 1/255, alpha: 1.0){
        didSet {
            textColor = textColorDefault
        }
    }
    
    @IBInspectable var cornerRadiusCustom: Double = 5 {
        didSet {
            self.layer.cornerRadius = CGFloat(cornerRadiusCustom)
        }
    }
    
    @IBInspectable var labelTitle: String = "" {
        didSet {
            //self.labelTitle = labelTitle
        }
    }
    
    func setup() {
        self.backgroundColor = backColor
        setValue(placeHolderColor, forKeyPath: "_placeholderLabel.textColor")
        textColor = textColorDefault
        self.layer.cornerRadius = CGFloat(cornerRadiusCustom)
        
        self.addLineToView(view: self, position:.LINE_POSITION_BOTTOM, color: UIColor.lightGray, width: 1.0)
        //self.textColor = UIColor.white
        
        let label = UILabel(frame: CGRect(x: 0, y: -20, width: 200, height: 21))
        //label.center = CGPoint(x: 160, y: 285)
        label.textAlignment = .left
        label.text = labelTitle
        label.font = label.font.withSize(13)
        label.textColor = UIColor.lightGray
        //self.addSubview(label)
    }
    
    override func awakeFromNib() {
        setup()
    }
    
    override func prepareForInterfaceBuilder() {
        setup()
    }
    
    
    
    
    enum LINE_POSITION {
        case LINE_POSITION_TOP
        case LINE_POSITION_BOTTOM
    }
    
    func addLineToView(view : UIView, position : LINE_POSITION, color: UIColor, width: Double) {
        let lineView = UIView()
        lineView.backgroundColor = color
        lineView.translatesAutoresizingMaskIntoConstraints = false // This is important!
        view.addSubview(lineView)
        
        let metrics = ["width" : NSNumber(value: width)]
        let views = ["lineView" : lineView]
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[lineView]|", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))
        
        switch position {
        case .LINE_POSITION_TOP:
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[lineView(width)]", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))
            break
        case .LINE_POSITION_BOTTOM:
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[lineView(width)]|", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))
            break
        default:
            break
        }
    }
    
}
