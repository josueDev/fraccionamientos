//
//  CustomTextFieldPicker.swift
//  atiende
//
//  Created by Josué :D on 18/10/18.
//  Copyright © 2018 Christian Vargas Saavedra. All rights reserved.
//

import UIKit

@IBDesignable class CustomButtonPicker: UIButton {
    
    @IBInspectable var backColor: UIColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 0.5) {
        didSet {
            self.backgroundColor = backColor
        }
    }
    
    @IBInspectable var cornerRadiusCustom: Double = 5 {
        didSet {
            self.layer.cornerRadius = CGFloat(cornerRadiusCustom)
        }
    }
    
    @IBInspectable var borderWidthCustom: Double = 1 {
        didSet {
            self.layer.borderWidth = CGFloat(borderWidthCustom)
        }
    }
    
    @IBInspectable var borderColorCustom:  UIColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1.0) {
        didSet {
            self.layer.borderColor = borderColorCustom.cgColor
        }
    }
    
    func setup() {
        //self.layer.cornerRadius = CGFloat(cornerRadiusCustom)
        //self.layer.borderWidth = CGFloat(borderWidthCustom)
        //self.layer.borderColor = borderColorCustom.cgColor
        
         self.addLineToView(view: self, position:.LINE_POSITION_BOTTOM, color: UIColor.lightGray, width: 1.0)
        
        
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        //let screenHeight = screenSize.height
        
        let image: UIImageView = UIImageView(frame: CGRect(x: screenWidth - 65, y: 2.5, width: 20, height: 20))
        image.image = UIImage(named: "ImageDownArrow")
        //image.backgroundColor = UIColor.red
        self.addSubview(image)
        //image.image = newValue
        //left.contentMode = .center
        
        
    }
    
    override func awakeFromNib() {
        setup()
    }
    
    override func prepareForInterfaceBuilder() {
        setup()
    }
    
    
    
    enum LINE_POSITION {
        case LINE_POSITION_TOP
        case LINE_POSITION_BOTTOM
    }
    
    func addLineToView(view : UIView, position : LINE_POSITION, color: UIColor, width: Double) {
        let lineView = UIView()
        lineView.backgroundColor = color
        lineView.translatesAutoresizingMaskIntoConstraints = false // This is important!
        view.addSubview(lineView)
        
        let metrics = ["width" : NSNumber(value: width)]
        let views = ["lineView" : lineView]
        view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[lineView]|", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))
        
        switch position {
        case .LINE_POSITION_TOP:
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[lineView(width)]", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))
            break
        case .LINE_POSITION_BOTTOM:
            view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[lineView(width)]|", options:NSLayoutConstraint.FormatOptions(rawValue: 0), metrics:metrics, views:views))
            break
        default:
            break
        }
    }
}

